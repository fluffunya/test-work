const path = require('path');
const autoprefixer = require('autoprefixer');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');

const isProduction = process.env.NODE_ENV === 'production';

module.exports = {
	cache: true,
	devtool: !isProduction ? 'eval-source-map' : false,
	entry: path.resolve(__dirname, 'client/index.js'),
	output: {
		path: path.resolve(__dirname, 'public'),
		publicPath: './',
		filename: isProduction ? 'bundle.[hash].js' : 'bundle.js'
	},
	resolve: {
		extensions: [".js", ".json", ".mjs"],
		alias: {
            components: path.resolve(__dirname, 'client/components'),
            api: path.resolve(__dirname, 'client/api'),
            pages: path.resolve(__dirname, 'client/pages'),
		}
	},
	devServer: {
		hot: true,
		historyApiFallback: true,
		contentBase: path.join(__dirname, "public"),
		compress: true,
		publicPath: '/',
        watchContentBase: true,
        port: 8000,
        proxy: {
          '/api': {
            target: 'http://localhost:3000',
          }
      }
	},
	module: {
		rules: [
			{
				test:  /\.(js|jsx)$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
			},
      {
        test: /\.(css|scss|sass)/,
        use: ['style-loader',
          {
            loader: 'css-loader',
          },
          {
            loader: `postcss-loader`,
            options: {
              options: {},
              plugins: () => {
                autoprefixer({ browsers: [ 'last 2 versions' ] });
              }
            }
          },
        ]
      },
			{
				test: /\.(jpg|jpeg|gif|png|svg)$/,
				loader: 'url-loader',
				exclude: /node_modules/,
				options: {
					limit: 10000
				}
			},
			{
				test: /\.(eot|ttf|woff2?)$/,
				loader: 'url-loader',
				options: {
					limit: 10000,
					name: '[name].[ext]'
				}
			},
		]
	},

	plugins: [
		new HtmlWebpackPlugin({
			alwaysWriteToDisk: true,
			inject: 'body',
			template: path.resolve(__dirname, 'client/index.html'),
			filename: 'index.html',
			appMountId: 'app',
			minify: {
				html5: true,
				collapseWhitespace: true
			},
		}),
		new HtmlWebpackHarddiskPlugin(),
		new webpack.NamedModulesPlugin(),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify(process.env.NODE_ENV)
			}
		}),
		new webpack.LoaderOptionsPlugin({
			minimize: true,
			debug: true,
			sourceMap: true,
		}),
       new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru/),
	],
};

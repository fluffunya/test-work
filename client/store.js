import { init } from '@rematch/core';
import sagaPlugin from 'rematch-saga';
import game from 'components/game/model';
import stepLogs from 'components/step-logs/model';
import score from 'components/score/model';



const store = init({
    plugins: [sagaPlugin()],
	models: {
        game,
        stepLogs,
        score
    },
})

export default store

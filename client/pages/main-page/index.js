import React from 'react';
import { Link } from 'react-router-dom';

const MainPage = () => {
    return (
        <div>
            <Link to={'/game'}>Начать игру</Link>
        </div>
    )
}

export default MainPage;

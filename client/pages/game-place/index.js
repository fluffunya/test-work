import React from 'react';
import Game from 'components/game';
import Score from 'components/score';
import StepLogs from 'components/step-logs';
import { useStyles } from './style';


const GamePlace = () => {
    const cn = useStyles();
    return (
        <div className={cn.wrap}>
            <div>
                <Game />
                <Score />
            </div>
            <StepLogs />
        </div>
    )
};

export default GamePlace;

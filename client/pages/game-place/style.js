import { createUseStyles } from "react-jss";

export const useStyles = createUseStyles({
    '@global': {
        body: {
            margin: '0',
            padding: '0'
        }
    },
    wrap: {
        minHeight: 'calc(100vh)',
        backgroundColor: '#352a3c',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        verticalAlign: 'middle'
    }
});

import React from 'react';
import ReactDOM from 'react-dom';
import Core from './core';

const app = <Core />;

ReactDOM.render(app, document.getElementById('app'));

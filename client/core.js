import React from 'react';
import { Switch, Route } from 'react-router';
import { Provider, connect } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import store from './store';
import { createBrowserHistory } from 'history';
import MainPage from 'pages/main-page';
import GamePlace from 'pages/game-place';

export default () => (
    <Provider store={store}>
        <Router>
            <Switch>
                <Route path="/game" component={GamePlace} />
                <Route exact path="/" component={MainPage} />
            </Switch>
        </Router>
    </Provider>
);

import React, {useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useStyles } from '../style';

const CubeItem = ({item, index}) => {
    const player = useSelector(state => state.game.player);
    const { game } = useDispatch();
    const value = item !== index ? item : '';
    const cn = useStyles();

    return (
        <div onClick={() => game.setMove(String(index))} className={cn.cubeItem}>{value}</div>
    )
}

export default CubeItem;

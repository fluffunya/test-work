import { requestGame, requestSetMove } from 'api';

const game = {
	state: {
        player: '',
        ai: '',
        nextMove: '',
        board: [],
        end: false
    },
	reducers: {
		getData(state, payload) {
            const data = [];
            payload.board.forEach((item, i) => {
                item.forEach((el, index) => {
                    data.push(el);
                })
            });

			return {
                ...state,
                player: payload.player,
                ai: payload.ai,
                nextMove: payload.nextMove,
                board: data,
                end: payload.end || false
            }
		},
	},
	effects: dispatch => ({
        async fetchGame() {
            const res = await requestGame();
            this.getData(res.data.result);
        },
        async setMove(data) {
            const res = await requestSetMove({index: data});
            this.getData(res.data.result);
        },
	}),
}

export default game

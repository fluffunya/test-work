import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import CubeItem from './parts/cube-item';
import { useStyles } from './style';

const Game = () => {
    const { game } = useDispatch();
    const board = useSelector(state => state.game.board);
    useEffect(() => {
        game.fetchGame();
    }, []);

    const cn = useStyles();
    return (
        <div>
            <div className={cn.cubeWrap}>
                {board.map((el, key) => {
                    return (
                        <CubeItem
                            key={key}
                            index={key + 1}
                            item={el}
                        />
                    )
                })}
            </div>
        </div>
    )
}

export default Game;

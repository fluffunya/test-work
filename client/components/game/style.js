import { createUseStyles } from "react-jss";

export const useStyles = createUseStyles({
    cubeWrap: {
        display: 'grid',
        gridTemplateColumns: '100px 100px 100px',
        width: '320px',
        rowGap: '10px',
        gridColumnGap: '10px'
    },
    cubeItem: {
        width: '100px',
        height: '100px',
        backgroundColor: '#bebebe',
    }
});

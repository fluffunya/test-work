import { fetchData } from 'api';

const score = {
	state: {
        data: {}
    },
	reducers: {
		increment(state, payload) {
			return {
                ...state,
                data: payload
            }
		},
	},
	effects: dispatch => ({
		async fetch(payload, rootState) {
			const data = await fetchData();
			this.increment(data);
		},
	}),
}

export default score

import React from 'react';
import { useStyles } from './style';

const Score = () => {
    const cn = useStyles();

    return (
        <div className={cn.wrap}>1-1</div>
    )
}

export default Score;

import axios from 'axios';

const instance = axios.create({
  baseURL: '',
});

export const requestGame = () => {
    return instance.get('/api/game');
}

export const requestSetMove = (params = {}) => {
    return instance.post('/api/game/move', params);
}
